#!/bin/bash

if [[ `uname -s` == Darwin ]]; then
	CWD=`dirname $(realpath $0)`
else
	CWD=`dirname $(readlink -f $0)`
fi

# download z x y
download() {
	mkdir -p "$CWD/$1/$2"
	wget -c https://a.tile.openstreetmap.org/$1/$2/$3.png -O "$CWD/$1/$2/$3.png"
}

# download_level z
download_level() {
	N=`echo "2^$1" | bc`
	for (( i=0;$i<$N;i=$i+1 )); do
		for (( j=0;$j<$N;j=$j+1 )); do
			download $1 $i $j
		done
	done
}

for z in 0 1 2 3 4 5 6 7; do
	download_level $z
done
