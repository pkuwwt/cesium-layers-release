
import os
import requests
import json

INIT_TOKEN='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkN2UwMzFmOC1mY2E3LTQ1ZDQtYjg4NC1mZDhlMzZmZjk4NTkiLCJpZCI6MjU5LCJpYXQiOjE2Mjc5MzM4NTJ9.Rv6Icz_hBcEDbR160A3jPJQfhdZKWo_MJ6KovN7HRpw'


def get_json(url):
    return requests.get(url).json()


def get_binary(url, headers={}):
    return requests.get(url, headers=headers).content


def write_binary(data, filename):
    with open(filename, 'wb') as f:
        f.write(data)

def get_ion(index, init_token):
    url='https://api.cesium.com/v1/assets/{}/endpoint?access_token={}'.format(index, init_token)
    return get_json(url)


class TerrainDownloader:
    def __init__(self, index=1, token=INIT_TOKEN, resume=True):
        self.index = index
        self.token = token
        self.ion = get_ion(index, token)
        self.resume = resume

    def renew_token(self):
        print('Renew token...')
        self.ion = get_ion(self.index, self.token)

    def _get_binary(self, url, tries=5):
        if tries <= 0:
            return None
        try:
            accessToken = self.ion['accessToken']
            return get_binary(url, {'authorization': 'Bearer ' + accessToken})
        except:
            self.renew_token()
            return self._get_binary(url, tries-1)


    def get_layer_json(self):
        url = os.path.join(self.ion['url'], 'layer.json')
        return self._get_binary(url)

    def get_terrain(self, z, x, y):
        url = os.path.join(self.ion['url'], '{}/{}/{}.terrain'.format(z, x, y))
        return self._get_binary(url)

    def download_layer_json(self, root):
        path = os.path.join(root, 'layer.json')
        if self.resume and os.path.isfile(path):
            return
        data = self.get_layer_json()
        write_binary(data, path)

    def download_terrain(self, z, x, y, root):
        dir = os.path.join(root, '{}/{}'.format(z, x))
        path = os.path.join(root, '{}/{}/{}.terrain'.format(z, x, y))
        if self.resume and os.path.isfile(path):
            return
        os.makedirs(dir, exist_ok=True)
        data = self.get_terrain(z, x, y)
        write_binary(data, path)

    def download_terrain_for_level(self, z, root):
        self.download_layer_json(root)
        path = os.path.join(root, 'layer.json')
        layer = json.loads(open(path).read())
        for range_ in layer['available'][z]:
            minx = range_['startX']
            maxx = range_['endX']
            miny = range_['startY']
            maxy = range_['endY']
            for x in range(minx, maxx+1):
                for y in range(miny, maxy+1):
                    print('Download {}/{}/{}.terrain'.format(z, x, y))
                    self.download_terrain(z, x, y, root)


if __name__ == '__main__':
    downloader = TerrainDownloader()
    downloader.download_layer_json('.')
    downloader.download_terrain_for_level(0, '.')
    downloader.download_terrain_for_level(1, '.')
    downloader.download_terrain_for_level(2, '.')
    downloader.download_terrain_for_level(3, '.')
    downloader.download_terrain_for_level(4, '.')
