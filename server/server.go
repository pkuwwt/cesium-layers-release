package main

import (
	"os"
	"path"
    "fmt"
    "log"
	"strconv"
    "net/http"
)

// isDirectory determines if a file represented
// by `path` is a directory or not
func IsDir(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	return fileInfo.IsDir()
}

func ReadUserIP(r *http.Request) string {
    IPAddress := r.Header.Get("X-Real-Ip")
    if IPAddress == "" {
        IPAddress = r.Header.Get("X-Forwarded-For")
    }
    if IPAddress == "" {
        IPAddress = r.RemoteAddr
    }
    return IPAddress
}

func main() {

	port := 8080
	dir := "."
	if (len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "--help")) {
		fmt.Println("USAGE: " + os.Args[0] + " [-h|--help]")
		fmt.Println("       " + os.Args[0] + " port [dir]")
		return
	}
	if (len(os.Args) > 1) {
		port1,err := strconv.Atoi(os.Args[1])
		if err == nil {
			port = port1
		} else {
			return
		}
	}
	if (len(os.Args) > 2) {
		dir = os.Args[2]
		if !IsDir(dir) {
			fmt.Println(dir + " is not a dir")
			return
		}
	}
	if dir == "." {
		fmt.Println("Serving current directory at 0.0.0.0:" + strconv.Itoa(port))
	} else {
		fmt.Println("Serving directory " + dir + " at 0.0.0.0:" + strconv.Itoa(port))
	}

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(ReadUserIP(r) + " " + r.URL.Path)
        http.ServeFile(w, r, path.Join(dir, r.URL.Path[1:]))
    })

    http.HandleFunc("/hi", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "Hi")
    })

    log.Fatal(http.ListenAndServe("0.0.0.0:" + strconv.Itoa(port), nil))
}

